##
## Copyright © 2018 Ehsan Tofigh
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
## http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
## Project: libexpr
##
## @file  .gitlab-ci.yml
## @brief Gitlab CI configuration script
##

image: registry.gitlab.com/etofigh/bob:latest

stages:
  - check
  - build
  - deploy

before_script:
  - git submodule sync
  - git submodule update --init --recursive

#
# Check stage jobs:
#
check:style:
  stage: check
  script:
    - cmake -H. -Bbuild && cd build
    - make style-check

check:cppcheck:
  stage: check
  script:
    - cmake -H. -Bbuild && cd build
    - make cpp-check
  allow_failure: true

#
# Build stage jobs:
#
build:linux:
  stage: build
  script:
    - cmake -H. -Bbuild && cd build
    - make
  artifacts:
    paths:
      - build
    expire_in: 1 hour

build:win:
  stage: build
  script:
    - cmake -H. -Bbuild -DCMAKE_TOOLCHAIN_FILE=bob/targets/windows.cmake && cd build
    - make
  artifacts:
    paths:
      - build
    expire_in: 1 hour

build:doc:
  stage: build
  script:
    - cmake -H. -Bbuild && cd build
    - make gen-doxygen
    - make gen-changelog
  artifacts:
    paths:
      - build
    expire_in: 1 hour

#
# Deploy stage jobs:
#
deploy:pages:
  stage: deploy
  dependencies:
    - build:doc
  script:
    - mv build/doc/doxygen/html/ public/
  artifacts:
    paths:
      - public
    expire_in: 30 days

deploy:linux:
  stage: deploy
  variables:
    PKGDIR: "${CI_PROJECT_NAME}-${CI_COMMIT_SHA}"
  dependencies:
    - build:linux
    - build:doc
  script:
    - mkdir -p $PKGDIR/lib
    - cp build/libexpr.a $PKGDIR/lib
    - mv build/doc/changelog.txt $PKGDIR
  artifacts:
    when: on_success
    name: $PKGDIR
    paths:
      - $PKGDIR

deploy:win:
  stage: deploy
  variables:
    PKGDIR: "${CI_PROJECT_NAME}-${CI_COMMIT_SHA}"
  dependencies:
    - build:win
    - build:doc
  script:
    - mkdir -p $PKGDIR/lib
    - cp build/libexpr.a $PKGDIR/lib
    - mv build/doc/changelog.txt $PKGDIR
  artifacts:
    when: on_success
    name: $PKGDIR
    paths:
      - $PKGDIR
