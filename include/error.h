/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  include/error.h
 * @brief Header file for libexpr error functions and structures
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Use C linkage to avoid name mangling in C++: */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * Enumerate all possible error states, including internal errors.
 */
enum lbx_error_code {
	LBX_ERROR_NONE,
	LBX_ERROR_INTERNAL,
	LBX_ERROR_DUPLICATE,
	LBX_ERROR_PARAM,
	LBX_ERROR_MALLOC,
	LBX_ERROR_RANGE,
	LBX_ERROR_PARSE,
	LBX_ERROR_COMPILE,

	LBX_ERROR_COUNT
};

/**
 * Structure to store the details of the error state.
 */
struct lbx_error_info {

	/** Human readable error string. */
	const char *msg;

	/** Expression to parse. */
	const char *expr;

	/**
	 * Offset of offending token in the expression (in characters). This is
	 * set to -1 if not applicable.
	 */
	int offset;

	/** Error code. */
	enum lbx_error_code code;
};

/**
 * Definition of the error callback type. User can provide an error handling
 * function with matching definition to be able to handle the low level error
 * processing.
 *
 * @param[in] error     Pointer to the `struct lbx_error_info' describing the
 *                      error.
 * @param[in] user_data User-defined opaque pointer which the library user may
 *                      register and which will be passed to the callback
 *                      function.
 *
 * @return None.
 */
typedef void (lbx_error_fn) (const struct lbx_error_info *error,
			     void                        *user_data);

/* Close C linkage directive for C++: */
#ifdef __cplusplus
}
#endif
