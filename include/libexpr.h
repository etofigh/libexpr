/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  include/libexpr.h
 * @brief libexpr's public library interface header.
 */

#pragma once

#include "error.h"
#include "operator.h"

#include <stdbool.h>

/* Use C linkage to avoid name mangling in C++: */
#ifdef __cplusplus
extern "C" {
#endif

/* Forward declare opaque library handle structure: */
struct lbx_handle;

/**
 * Create an opaque handle object for libexpr. To use libexpr, the user must
 * register user-defined error handler, operators and variables to this handle
 * using the appropriate functions, then call `lbx_compile()' on the handle to
 * start the compilation process. Afterwards, a call to `lbx_evaluate()' will
 * evaluate the compiled expression.
 *
 * @return NULL on failure, else a valid pointer to a library handle.
 */
extern struct lbx_handle *lbx_create (void);

/**
 * Release all resources registered to an opaque libexpr handle, then destroy
 * the handle object.
 *
 * @param[in] handle Pointer to opaque library handle structure.
 *
 * @return None
 */
extern void lbx_destroy (struct lbx_handle *handle);

/**
 * Register an error callback function inside a libexpr handle. Setting the
 * callback to NULL will unregister an existing error callback.
 *
 * @param[out] handle    Pointer to opaque library handle structure.
 * @param[in]  callback  Pointer to error handling function to register, or NULL.
 * @param[in]  user_data User-defined pointer to use for callback call.
 *
 * @return None
 */
extern void lbx_register_error_callback (struct lbx_handle *handle,
					 lbx_error_fn      *callback,
					 void              *user_data);

/**
 * Registers a user-defined operator inside a libexpr handle.
 *
 * @param[in,out] handle Pointer to opaque library handle structure.
 * @param[in]     op     Pointer to a lbx_operator structure defining the
 *                       user-defined operator to register.
 *
 * @retval true  Successfully registered the operator.
 * @retval false Failed to register the operator. For greater level of details,
 *               the user should register an error callback function before
 *               calling this function.
 */
extern bool lbx_register_operator (struct lbx_handle         *handle,
				   const struct lbx_operator *op);

/**
 * Registers a user-defined variable inside a libexpr handle.
 *
 * @param[in,out] handle Pointer to opaque library handle structure.
 * @param[in]     str    Variable's name.
 * @param[in]     ptr    Pointer to the variable's location.
 *
 * @retval true  Successfully registered the variable.
 * @retval false Failed to register the variable. For greater level of details,
 *               the user should register an error callback function before
 *               calling this function.
 */
extern bool lbx_register_variable (struct lbx_handle *handle,
				   const char        *str,
				   double            *ptr);

/**
 * Compiles the math expression in the provided string into an internal AST
 * format. This makes it possible evaluate the expression efficiently here after.
 *
 * @param[in,out] handle Pointer to opaque library handle structure.
 * @param[in]     str    Pointer to a null-terminated string containing the math
 *                       expression to compile.
 *
 * @retval true  Successfully compiled the provided expression.
 * @retval false Failed to compile the provided expression. For greater level of
 *               details, the user should register an error callback function
 *               before calling this function.
 */
extern bool lbx_compile (struct lbx_handle *handle, const char *str);

/**
 * Evaluates a compiled math expression and provides the results.
 *
 * @param[in]  handle Pointer to opaque library handle structure.
 * @param[out] result Pointer to a double to hold the results of the expression.
 *
 * @retval true  Successfully evaluated the expression.
 * @retval false Failed to evaluate the expression. For greater level of
 *               details, the user should register an error callback function
 *               before calling this function.
 */
extern bool lbx_evaluate (struct lbx_handle *handle, double *result);

/* Close C linkage directive for C++: */
#ifdef __cplusplus
}
#endif
