/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  include/operator.h
 * @brief Header file for operator functions and structures.
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Use C linkage to avoid name mangling in C++: */
#ifdef __cplusplus
extern "C" {
#endif

/**
 * Maximum allowable operator precedence. Higher precedence numbers take
 * priority over lower ones.
 */
#define LBX_PRECEDENCE_MAX 100U

/**
 * Operator associativity: the associativity of an operator is a property that
 * determines how operators of the same precedence are grouped in the absence of
 * parentheses.
 */
enum lbx_associativity {
	LBX_ASSOCIATIVITY_LEFT,
	LBX_ASSOCIATIVITY_RIGHT,
	LBX_ASSOCIATIVITY_NONE,

	LBX_ASSOCIATIVITY_COUNT
};

/**
 * Definition of the operator evaluation function type. The user can register
 * custom operator evaluation functions with matching definition to be able to
 * define how new operators are evaluated.
 *
 * @param[in] operands An array of operands.
 *
 * @return Result of the operation or function.
 */
typedef double (*lbx_operator_fn) (const double *operands);

/**
 * Structure to define an operator.
 */
struct lbx_operator {

	/** String representation of the operator. */
	const char *str;

	/** Pointer to the operator's evaluation function. */
	lbx_operator_fn eval;

	/** The number of operands required for the evaluation function. */
	uint8_t n_operands;

	/** Operator associativity. */
	enum lbx_associativity associativity;

	/** Operator precedence. Higher numbers have higher precedence. */
	uint32_t precedence;

	/** If true, the operator is a function. */
	bool is_func;
};

/* Close C linkage directive for C++: */
#ifdef __cplusplus
}
#endif
