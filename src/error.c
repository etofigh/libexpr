/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/error.c
 * @brief Internal error handling functions.
 */

#include "error.h"

#include <stdio.h>
#include <stdlib.h>

void lbx_error_raise (const struct lbx_handle  *handle,
		      const enum lbx_error_code code,
		      const char               *token,
		      const char               *fmt,
		      ...)
{
	int     len;
	char   *msg = NULL;
	va_list va;

	/* Need a handle, a format string and a registered callback function: */
	if (handle == NULL || fmt == NULL || handle->error.fn == NULL ||
	    code >= LBX_ERROR_COUNT) {
		return;
	}

	/* Grab variable args: */
	va_start(va, fmt);

	/* Dummy run of vsnprintf to get the buffer size: */
	if ((len = vsnprintf(msg, 0U, fmt, va)) <= 0) {
		goto err;
	}

	/* Reset variable args: */
	va_end(va);
	va_start(va, fmt);

	/* Allocate space for the error string and the zero terminator: */
	if ((msg = malloc((size_t) len + 1U)) == NULL) {
		goto err;
	}

	/* Run vsnprintf again to populate the message buffer: */
	if (vsnprintf(msg, (size_t) len + 1U, fmt, va) != len) {
		goto err;
	}

	/* Report the offset of the problematic token if one is provided: */
	int offset = -1;
	if (token != NULL) {
		ptrdiff_t diff = token - handle->parser.expr;
		offset = (int) diff;
	}

	/* Create an error descriptor structure: */
	const struct lbx_error_info error = {
		.msg    = msg,
		.expr   = handle->parser.expr,
		.offset = offset,
		.code   = code,
	};

	/* Call the user's error callback: */
	handle->error.fn(&error, handle->error.user_data);

err:
	free(msg);
	va_end(va);
}
