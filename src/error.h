/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/handle.h
 * @brief Header file to define library handle structure.
 */

#pragma once

#include "handle.h"

#include <error.h>

#include <stdarg.h>

/**
 * Raise an error by calling the user callback, if registered.
 *
 * @param[in] handle Pointer to library handle structure.
 * @param[in] code   Error code to register.
 * @param[in] token  Pointer to the token in the expression causing the error.
 *                   Can optionally be set to NULL.
 * @param[in] fmt    Format string for error message.
 * @param[in] ...    Arguments for format string.
 *
 * @return None.
 */
__attribute__ ((format(printf, 4, 5)))
extern void lbx_error_raise (const struct lbx_handle  *handle,
			     const enum lbx_error_code code,
			     const char               *token,
			     const char               *fmt,
			     ...);

/**
 * Macro to simplify error function interface.
 */
#define lbx_error(HANDLE, CODE, TOKEN, MSG, ...) \
	lbx_error_raise((HANDLE), (CODE), (TOKEN), \
			"%s:%u: " MSG, __FUNCTION__, __LINE__, ## __VA_ARGS__)
