/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/handle.h
 * @brief Header file to define library handle structure.
 */

#pragma once

#include "token.h"

#include <libexpr.h>

#include <stdint.h>
#include <stddef.h>

/**
 * Maximum number of supported operators. This includes the internal operators.
 * This limits the number of operators that can be parsed.
 */
#define LBX_OPERATOR_LIST_SIZE 128U

/**
 * Maximum number of supported variables. This includes the internal variables.
 * This limits the number of variables that can be parsed.
 */
#define LBX_VARIABLE_LIST_SIZE 128U

/**
 * Maximum number of output tokens. Output tokens include variables, literals
 * and operators. This limits the complexity of expressions that can be parsed.
 */
#define LBX_OUTPUT_QUEUE_SIZE  (LBX_OPERATOR_LIST_SIZE + LBX_VARIABLE_LIST_SIZE)

/**
 * Structure that defines a state shared between the libexpr functions.
 */
struct lbx_handle {

	/** Structure containing information about the operators in use. */
	struct {

		/** Number of operators in use. */
		size_t num;

		/** List of operators. */
		struct lbx_operator list[LBX_OPERATOR_LIST_SIZE];
	} operators;

	/** Structure containing information about the variables in use. */
	struct {

		/** Number of variables in use. */
		size_t num;

		/** List of variables. */
		struct lbx_variable list[LBX_VARIABLE_LIST_SIZE];
	} variables;

	/** Structure containing information about the expression parser. */
	struct {

		/** Pointer to the string containing the expression to parse. */
		char *expr;

		/** Current boost level of the operator precedence. */
		uint32_t boost;

		/** Pointer to operator stack. */
		struct stack *op_stack;

		/** The type of last parsed token. */
		enum lbx_token_type last;
	} parser;

	/** Structure containing information about the output queue. */
	struct {

		/** Number of tokens in the output queue. */
		size_t num;

		/** The output queue. */
		struct lbx_token queue[LBX_OUTPUT_QUEUE_SIZE];
	} output;

	/** Structure to define error properties and handling. */
	struct {

		/** Function pointer to error handling callback function. */
		lbx_error_fn *fn;

		/** User-defined pointer to use for callback call. */
		void *user_data;

	} error;
};
