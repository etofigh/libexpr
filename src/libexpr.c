/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/libexpr.c
 * @brief libexpr's implementation.
 */

#define _POSIX_C_SOURCE 200809L

#include "error.h"
#include "optimise.h"
#include "parse.h"
#include "stack.h"
#include "str.h"

#include "ops/arith.h"
#include "ops/bit.h"
#include "ops/cnvt.h"
#include "ops/cmp.h"
#include "ops/func.h"
#include "ops/logic.h"
#include "ops/trig.h"

#include <libexpr.h>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct lbx_handle *lbx_create (void)
{
	struct lbx_handle *handle;

	/* Allocate memory for the library handle: */
	if ((handle = calloc(1U, sizeof (*handle))) == NULL) {
		return NULL;
	}

	/* Initialize the library handle: */
	handle->operators.num = 0U;
	handle->variables.num = 0U;

	/* Register default operators: */
	arith_register(handle);
	bit_register(handle);
	cnvt_register(handle);
	cmp_register(handle);
	func_register(handle);
	logic_register(handle);
	trig_register(handle);

	return handle;
}

void lbx_destroy (struct lbx_handle *handle)
{
	if (handle == NULL) {
		return;
	}

	/* Release the expression handle itself: */
	free(handle);
}

void lbx_register_error_callback (struct lbx_handle *handle,
				  lbx_error_fn      *callback,
				  void              *user_data)
{
	if (handle == NULL) {
		return;
	}

	handle->error.fn        = callback;
	handle->error.user_data = user_data;
}

bool lbx_register_operator (struct lbx_handle         *handle,
			    const struct lbx_operator *op)
{
	if (handle == NULL) {
		return false;
	}

	if (op == NULL || op->str == NULL || op->precedence >= LBX_PRECEDENCE_MAX ||
	    op->associativity >= LBX_ASSOCIATIVITY_COUNT) {
		lbx_error(handle, LBX_ERROR_PARAM, NULL, "Invalid operator parameters.");
		return false;
	}

	if (handle->operators.num == LBX_OPERATOR_LIST_SIZE) {
		lbx_error(handle, LBX_ERROR_RANGE, NULL,
			  "Maximum number of operators '%u' reached.",
			  LBX_OPERATOR_LIST_SIZE);
		return false;
	}

	if (lbx_str_is_operator(handle, op->str, SIZE_MAX) == true) {
		lbx_error(handle, LBX_ERROR_DUPLICATE, NULL, "Operator already defined.");
		return false;
	}

	handle->operators.list[handle->operators.num++] = *op;
	return true;
}

bool lbx_register_variable (struct lbx_handle *handle,
			    const char        *str,
			    double            *ptr)
{
	if (handle == NULL) {
		return false;
	}

	if (str == NULL || ptr == NULL) {
		lbx_error(handle, LBX_ERROR_PARAM, NULL, "Invalid variable parameters.");
		return false;
	}

	if (handle->variables.num == LBX_VARIABLE_LIST_SIZE) {
		lbx_error(handle, LBX_ERROR_RANGE, NULL,
			  "Maximum number of variables '%u' reached.",
			  LBX_VARIABLE_LIST_SIZE);
		return false;
	}

	if (lbx_str_is_variable(handle, str, SIZE_MAX) == true) {
		lbx_error(handle, LBX_ERROR_DUPLICATE, NULL, "Variable already defined.");
		return false;
	}

	struct lbx_variable var = { .str = str, .ptr = ptr };
	handle->variables.list[handle->variables.num++] = var;
	return true;
}

bool lbx_compile (struct lbx_handle *handle, const char *str)
{
	if (handle == NULL) {
		return false;
	}

	handle->parser.expr     = NULL;
	handle->parser.op_stack = NULL;

	if (str == NULL) {
		lbx_error(handle, LBX_ERROR_PARAM, NULL, "Invalid parameters.");
		goto err;
	}

	/* Initialize the compiler: */
	handle->output.num   = 0U;
	handle->parser.boost = 0U;
	handle->parser.expr  = strdup(str);
	handle->parser.last  = LBX_TOKEN_OPERATOR;

	if (handle->parser.expr == NULL) {
		lbx_error(handle, LBX_ERROR_MALLOC, NULL,
			  "Failed to allocate memory for the expression string.");
		goto err;
	}

	/* Trim the white space characters: */
	lbx_str_trim(handle->parser.expr);

	/* Allocate memory for the operator stack: */
	if ((handle->parser.op_stack = stack_alloc(32U, sizeof (struct lbx_operator))) == NULL) {
		lbx_error(handle, LBX_ERROR_MALLOC, NULL,
			  "Failed to allocate memory for the operator stack.");
		goto err;
	}

	/* Parse the expression into the output queue: */
	char *token = handle->parser.expr;
	while (*token && parse_token(handle, token, &token));

	/* Unmatched paranthesis: */
	if (handle->parser.boost != 0U) {
		lbx_error(handle, LBX_ERROR_PARSE, NULL, "Unmatched paranthesis.");
		goto err;
	}

	/* The expression was not fully consumed: */
	if (*token != '\0') {
		lbx_error(handle, LBX_ERROR_COMPILE, token, "Unknown token.");
		goto err;
	}

	/* Pop all remaining operators onto the output queue: */
	struct lbx_token top = { .type = LBX_TOKEN_OPERATOR };
	while (stack_pop(handle->parser.op_stack, &top.token.op)) {
		handle->output.queue[handle->output.num++] = top;
	}

	/* Optimise the expression by performing constant folding: */
	if (optimise_constants(handle) == false) {
		lbx_error(handle, LBX_ERROR_INTERNAL, NULL, "Failed to optimize the expression.");
		goto err;
	}

	/* Clean up the compiler resources: */
	free(handle->parser.expr);
	stack_free(handle->parser.op_stack);
	return true;

err:
	handle->output.num = 0U;
	free(handle->parser.expr);
	stack_free(handle->parser.op_stack);
	return false;
}

bool lbx_evaluate (struct lbx_handle *handle, double *result)
{
	if (handle == NULL || result == NULL) {
		return false;
	}

	double  r;
	double  operands[10U];
	uint8_t n_operands = 0U;

	for (size_t i = 0U; i < handle->output.num; i++) {
		struct lbx_token *token = &handle->output.queue[i];

		switch (token->type) {
		case LBX_TOKEN_OPERATOR:
			if (n_operands < token->token.op.n_operands) {
				lbx_error(handle, LBX_ERROR_INTERNAL, NULL,
					  "Expected %u operands for '%s', only got '%u'.",
					  token->token.op.n_operands, token->token.op.str,
					  n_operands);
				return false;
			}
			n_operands -= token->token.op.n_operands;
			r = (*token->token.op.eval)(&operands[n_operands]);
			break;
		case LBX_TOKEN_LITERAL:
			r = token->token.literal;
			break;
		case LBX_TOKEN_VARIABLE:
			r = *token->token.var.ptr;
			break;
		default:
			lbx_error(handle, LBX_ERROR_INTERNAL, NULL, "Invalid token type.");
			return false;
		}

		operands[n_operands++] = r;
	}

	/* Sanity check to see if all operands have been consumed: */
	if (n_operands != 1U) {
		lbx_error(handle, LBX_ERROR_INTERNAL, NULL, "Syntax error: invalid expression.");
		return false;
	}

	*result = operands[0];
	return true;
}
