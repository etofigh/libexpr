/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/ops/arith.h
 * @brief Header file to define primitive arithmetic operators and functions.
 */

#pragma once

#include <operator.h>

#include <math.h>

/**
 * Operators:
 */
/* *INDENT-OFF* */
static double eval_add (const double *operands) { return operands[0] + operands[1];      }
static double eval_sub (const double *operands) { return operands[0] - operands[1];      }
static double eval_mul (const double *operands) { return operands[0] * operands[1];      }
static double eval_div (const double *operands) { return operands[0] / operands[1];      }
static double eval_mod (const double *operands) { return fmod(operands[0], operands[1]); }
static double eval_pow (const double *operands) { return pow(operands[0], operands[1]);  }

static struct lbx_operator ops_add = { "+",   &eval_add, 2U, LBX_ASSOCIATIVITY_LEFT, 11U, false };
static struct lbx_operator ops_sub = { "-",   &eval_sub, 2U, LBX_ASSOCIATIVITY_LEFT, 11U, false };
static struct lbx_operator ops_mul = { "*",   &eval_mul, 2U, LBX_ASSOCIATIVITY_LEFT, 12U, false };
static struct lbx_operator ops_div = { "/",   &eval_div, 2U, LBX_ASSOCIATIVITY_LEFT, 12U, false };
static struct lbx_operator ops_mod = { "%",   &eval_mod, 2U, LBX_ASSOCIATIVITY_LEFT, 12U, false };
static struct lbx_operator ops_pow = { "pow", &eval_pow, 2U, LBX_ASSOCIATIVITY_NONE, 32U, true  };
/* *INDENT-ON* */

/**
 * Registers all constants and operators for this module.
 *
 * @param[in] handle Pointer to library handle structure.
 *
 * @retval true  Successfully registered all constants and operators for this
 *               module.
 * @retval false Failed to register all constants and operators.
 */
static bool arith_register (struct lbx_handle *handle)
{
	bool ret = true;

	ret &= lbx_register_operator(handle, &ops_add);
	ret &= lbx_register_operator(handle, &ops_sub);
	ret &= lbx_register_operator(handle, &ops_mul);
	ret &= lbx_register_operator(handle, &ops_div);
	ret &= lbx_register_operator(handle, &ops_mod);
	ret &= lbx_register_operator(handle, &ops_pow);

	return ret;
}
