/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/ops/bit.h
 * @brief Header file to define bitwise operators and functions.
 */

#pragma once

#include <operator.h>

#include <stdint.h>

/**
 * Operators:
 */
/* *INDENT-OFF* */
static double eval_and (const double *operands) { return (uint32_t) operands[0] & (uint32_t) operands[1];  }
static double eval_or  (const double *operands) { return (uint32_t) operands[0] | (uint32_t) operands[1];  }
static double eval_xor (const double *operands) { return (uint32_t) operands[0] ^ (uint32_t) operands[1];  }
static double eval_not (const double *operands) { return ~((uint32_t) operands[0]);                        }
static double eval_lsl (const double *operands) { return (uint32_t) operands[0] << (uint32_t) operands[1]; }
static double eval_lsr (const double *operands) { return (uint32_t) operands[0] >> (uint32_t) operands[1]; }

static struct lbx_operator ops_and = { "&",  &eval_and, 2U, LBX_ASSOCIATIVITY_LEFT,  7U,  false };
static struct lbx_operator ops_or  = { "|",  &eval_or,  2U, LBX_ASSOCIATIVITY_LEFT,  5U,  false };
static struct lbx_operator ops_xor = { "^",  &eval_xor, 2U, LBX_ASSOCIATIVITY_LEFT,  6U,  false };
static struct lbx_operator ops_not = { "~",  &eval_not, 1U, LBX_ASSOCIATIVITY_RIGHT, 13U, false };
static struct lbx_operator ops_lsl = { "<<", &eval_lsl, 2U, LBX_ASSOCIATIVITY_LEFT,  10U, false };
static struct lbx_operator ops_lsr = { ">>", &eval_lsr, 2U, LBX_ASSOCIATIVITY_LEFT,  10U, false };
/* *INDENT-ON* */

/**
 * Registers all constants and operators for this module.
 *
 * @param[in] handle Pointer to library handle structure.
 *
 * @retval true  Successfully registered all constants and operators for this
 *				 module.
 * @retval false Failed to register all constants and operators.
 */
static bool bit_register (struct lbx_handle *handle)
{
	bool ret = true;

	ret &= lbx_register_operator(handle, &ops_and);
	ret &= lbx_register_operator(handle, &ops_or);
	ret &= lbx_register_operator(handle, &ops_xor);
	ret &= lbx_register_operator(handle, &ops_not);
	ret &= lbx_register_operator(handle, &ops_lsl);
	ret &= lbx_register_operator(handle, &ops_lsr);

	return ret;
}
