/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/ops/cmp.h
 * @brief Header file to define comparison operators and functions.
 */

#pragma once

#include <operator.h>

#include <math.h>

#define CMP_DOUBLE_EPSILON (1e-6)

/**
 * Operators:
 */
/* *INDENT-OFF* */
static double eval_eq   (const double *operands) { return fabs(operands[0] - operands[1]) <= CMP_DOUBLE_EPSILON; }
static double eval_neq  (const double *operands) { return fabs(operands[0] - operands[1]) > CMP_DOUBLE_EPSILON;  }
static double eval_gt   (const double *operands) { return operands[0] > operands[1];                             }
static double eval_gteq (const double *operands) { return operands[0] >= operands[1];                            }
static double eval_lt   (const double *operands) { return operands[0] < operands[1];                             }
static double eval_lteq (const double *operands) { return operands[0] <= operands[1];                            }

static struct lbx_operator ops_eq   = { "==", &eval_eq,   2U, LBX_ASSOCIATIVITY_LEFT, 8U, false };
static struct lbx_operator ops_neq  = { "!=", &eval_neq,  2U, LBX_ASSOCIATIVITY_LEFT, 8U, false };
static struct lbx_operator ops_gt   = { ">",  &eval_gt,   2U, LBX_ASSOCIATIVITY_LEFT, 8U, false };
static struct lbx_operator ops_gteq = { ">=", &eval_gteq, 2U, LBX_ASSOCIATIVITY_LEFT, 8U, false };
static struct lbx_operator ops_lt   = { "<",  &eval_lt,   2U, LBX_ASSOCIATIVITY_LEFT, 8U, false };
static struct lbx_operator ops_lteq = { "<=", &eval_lteq, 2U, LBX_ASSOCIATIVITY_LEFT, 8U, false };
/* *INDENT-ON* */

/**
 * Registers all constants and operators for this module.
 *
 * @param[in] handle Pointer to library handle structure.
 *
 * @retval true  Successfully registered all constants and operators for this
 *               module.
 * @retval false Failed to register all constants and operators.
 */
static bool cmp_register (struct lbx_handle *handle)
{
	bool ret = true;

	ret &= lbx_register_operator(handle, &ops_eq);
	ret &= lbx_register_operator(handle, &ops_neq);
	ret &= lbx_register_operator(handle, &ops_gt);
	ret &= lbx_register_operator(handle, &ops_gteq);
	ret &= lbx_register_operator(handle, &ops_lt);
	ret &= lbx_register_operator(handle, &ops_lteq);

	return ret;
}
