/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/ops/cnvt.h
 * @brief Header file to define conversion and casting operators and functions.
 */

#pragma once

#include <operator.h>

#include <stdint.h>

/**
 * Operators:
 */
/* *INDENT-OFF* */
static double eval_int8   (const double *operands) { return (uint8_t) operands[0];  }
static double eval_int16  (const double *operands) { return (int16_t) operands[0];  }
static double eval_int32  (const double *operands) { return (int32_t) operands[0];  }
static double eval_uint8  (const double *operands) { return (uint8_t) operands[0];  }
static double eval_uint16 (const double *operands) { return (uint16_t) operands[0]; }
static double eval_uint32 (const double *operands) { return (uint32_t) operands[0]; }

static struct lbx_operator ops_int8   = { "int8",   &eval_int8,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_int16  = { "int16",  &eval_int16,  1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_int32  = { "int32",  &eval_int32,  1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_uint8  = { "uint8",  &eval_uint8,  1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_uint16 = { "uint16", &eval_uint16, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_uint32 = { "uint32", &eval_uint32, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
/* *INDENT-ON* */

/**
 * Registers all constants and operators for this module.
 *
 * @param[in] handle Pointer to library handle structure.
 *
 * @retval true  Successfully registered all constants and operators for this
 *               module.
 * @retval false Failed to register all constants and operators.
 */
static bool cnvt_register (struct lbx_handle *handle)
{
	bool ret = true;

	ret &= lbx_register_operator(handle, &ops_int8);
	ret &= lbx_register_operator(handle, &ops_int16);
	ret &= lbx_register_operator(handle, &ops_int32);
	ret &= lbx_register_operator(handle, &ops_uint8);
	ret &= lbx_register_operator(handle, &ops_uint16);
	ret &= lbx_register_operator(handle, &ops_uint32);

	return ret;
}
