/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/ops/func.h
 * @brief Header file to define miscellaneous operators and functions.
 */

#pragma once

#include <operator.h>

#include <math.h>

/**
 * Operators:
 */
/* *INDENT-OFF* */
static double eval_abs   (const double *operands) { return fabs(operands[0]);                                       }
static double eval_ceil  (const double *operands) { return ceil(operands[0]);                                       }
static double eval_erf   (const double *operands) { return erf(operands[0]);                                        }
static double eval_erfc  (const double *operands) { return erfc(operands[0]);                                       }
static double eval_exp   (const double *operands) { return exp(operands[0]);                                        }
static double eval_expm1 (const double *operands) { return expm1(operands[0]);                                      }
static double eval_floor (const double *operands) { return floor(operands[0]);                                      }
static double eval_log   (const double *operands) { return log(operands[0]);                                        }
static double eval_log10 (const double *operands) { return log10(operands[0]);                                      }
static double eval_log1p (const double *operands) { return log1p(operands[0]);                                      }
static double eval_log2  (const double *operands) { return log2(operands[0]);                                       }
static double eval_max   (const double *operands) { return (operands[0] > operands[1]) ? operands[0] : operands[1]; }
static double eval_min   (const double *operands) { return (operands[0] < operands[1]) ? operands[0] : operands[1]; }
static double eval_round (const double *operands) { return round(operands[0]);                                      }
static double eval_sgn   (const double *operands) { return ((operands[0] > 0) - (operands[0] < 0));                 }
static double eval_sqrt  (const double *operands) { return sqrt(operands[0]);                                       }
static double eval_trunc (const double *operands) { return trunc(operands[0]);                                      }

static struct lbx_operator ops_abs   = { "abs",   &eval_abs,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_ceil  = { "ceil",  &eval_ceil,  1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_erf   = { "erf",   &eval_erf,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_erfc  = { "erfc",  &eval_erfc,  1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_exp   = { "exp",   &eval_exp,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_expm1 = { "expm1", &eval_expm1, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_floor = { "floor", &eval_floor, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_log   = { "log",   &eval_log,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_log10 = { "log10", &eval_log10, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_log1p = { "log1p", &eval_log1p, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_log2  = { "log2",  &eval_log2,  1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_max   = { "max",   &eval_max,   2U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_min   = { "min",   &eval_min,   2U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_round = { "round", &eval_round, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_sgn   = { "sgn",   &eval_sgn,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_sqrt  = { "sqrt",  &eval_sqrt,  1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_trunc = { "trunc", &eval_trunc, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
/* *INDENT-ON* */

/**
 * Registers all constants and operators for this module.
 *
 * @param[in] handle Pointer to library handle structure.
 *
 * @retval true  Successfully registered all constants and operators for this
 *               module.
 * @retval false Failed to register all constants and operators.
 */
static bool func_register (struct lbx_handle *handle)
{
	bool ret = true;

	ret &= lbx_register_operator(handle, &ops_abs);
	ret &= lbx_register_operator(handle, &ops_ceil);
	ret &= lbx_register_operator(handle, &ops_erf);
	ret &= lbx_register_operator(handle, &ops_erfc);
	ret &= lbx_register_operator(handle, &ops_exp);
	ret &= lbx_register_operator(handle, &ops_expm1);
	ret &= lbx_register_operator(handle, &ops_floor);
	ret &= lbx_register_operator(handle, &ops_log);
	ret &= lbx_register_operator(handle, &ops_log10);
	ret &= lbx_register_operator(handle, &ops_log1p);
	ret &= lbx_register_operator(handle, &ops_log2);
	ret &= lbx_register_operator(handle, &ops_max);
	ret &= lbx_register_operator(handle, &ops_min);
	ret &= lbx_register_operator(handle, &ops_round);
	ret &= lbx_register_operator(handle, &ops_sgn);
	ret &= lbx_register_operator(handle, &ops_sqrt);
	ret &= lbx_register_operator(handle, &ops_trunc);

	return ret;
}
