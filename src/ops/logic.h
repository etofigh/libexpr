/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/ops/logic.h
 * @brief Header file to define logic operators and functions.
 */

#pragma once

#include <operator.h>

#include <stdint.h>

/**
 * Operators:
 */
/* *INDENT-OFF* */
static double eval_land (const double *operands) { return (uint32_t) operands[0] && (uint32_t) operands[1]; }
static double eval_lor  (const double *operands) { return (uint32_t) operands[0] || (uint32_t) operands[1]; }
static double eval_lnot (const double *operands) { return !((uint32_t) operands[0]);                        }

static struct lbx_operator ops_land = { "&&", &eval_land, 2U, LBX_ASSOCIATIVITY_LEFT,  4U,  false };
static struct lbx_operator ops_lor  = { "||", &eval_lor,  2U, LBX_ASSOCIATIVITY_LEFT,  3U,  false };
static struct lbx_operator ops_lnot = { "!",  &eval_lnot, 1U, LBX_ASSOCIATIVITY_RIGHT, 13U, false };
/* *INDENT-ON* */

/**
 * Registers all constants and operators for this module.
 *
 * @param[in] handle Pointer to library handle structure.
 *
 * @retval true  Successfully registered all constants and operators for this
 *				 module.
 * @retval false Failed to register all constants and operators.
 */
static bool logic_register (struct lbx_handle *handle)
{
	bool ret = true;

	ret &= lbx_register_operator(handle, &ops_land);
	ret &= lbx_register_operator(handle, &ops_lor);
	ret &= lbx_register_operator(handle, &ops_lnot);

	return ret;
}
