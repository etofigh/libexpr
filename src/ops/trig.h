/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/ops/trig.h
 * @brief Header file to define trigonometry operators and functions.
 */

#pragma once

#include <operator.h>

#include <math.h>

/**
 * Constants:
 */
static double eval_pi = 3.1415926535897931;

/**
 * Operators:
 */
/* *INDENT-OFF* */
static double eval_acos    (const double *operands) { return acos(operands[0]);                   }
static double eval_acosh   (const double *operands) { return acosh(operands[0]);                  }
static double eval_asin    (const double *operands) { return asin(operands[0]);                   }
static double eval_asinh   (const double *operands) { return asinh(operands[0]);                  }
static double eval_atan    (const double *operands) { return atan(operands[0]);                   }
static double eval_atanh   (const double *operands) { return atanh(operands[0]);                  }
static double eval_atan2   (const double *operands) { return atan2(operands[0], operands[0]);     }
static double eval_cos     (const double *operands) { return cos(operands[0]);                    }
static double eval_cosh    (const double *operands) { return cosh(operands[0]);                   }
static double eval_cot     (const double *operands) { return cos(operands[0]) / sin(operands[0]); }
static double eval_csc     (const double *operands) { return 1.0 / sin(operands[0]);              }
static double eval_sec     (const double *operands) { return 1.0 / cos(operands[0]);              }
static double eval_sin     (const double *operands) { return sin(operands[0]);                    }
static double eval_sinc    (const double *operands) { return sin(operands[0]) / operands[0];      }
static double eval_sinh    (const double *operands) { return sinh(operands[0]);                   }
static double eval_tan     (const double *operands) { return tan(operands[0]);                    }
static double eval_tanh    (const double *operands) { return tanh(operands[0]);                   }
static double eval_rad2deg (const double *operands) { return operands[0] * (180.0 / eval_pi);     }
static double eval_deg2rad (const double *operands) { return operands[0] * (eval_pi / 180.0);     }

static struct lbx_operator ops_acos    = { "acos",    &eval_acos,    1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_acosh   = { "acosh",   &eval_acosh,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_asin    = { "asin",    &eval_asin,    1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_asinh   = { "asinh",   &eval_asinh,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_atan    = { "atan",    &eval_atan,    1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_atanh   = { "atanh",   &eval_atanh,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_atan2   = { "atan2",   &eval_atan2,   1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_cos     = { "cos",     &eval_cos,     1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_cosh    = { "cosh",    &eval_cosh,    1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_cot     = { "cot",     &eval_cot,     1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_csc     = { "csc",     &eval_csc,     1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_sec     = { "sec",     &eval_sec,     1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_sin     = { "sin",     &eval_sin,     1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_sinc    = { "sinc",    &eval_sinc,    1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_sinh    = { "sinh",    &eval_sinh,    1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_tan     = { "tan",     &eval_tan,     1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_tanh    = { "tanh",    &eval_tanh,    1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_rad2deg = { "rad2deg", &eval_rad2deg, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
static struct lbx_operator ops_deg2rad = { "deg2rad", &eval_deg2rad, 1U, LBX_ASSOCIATIVITY_NONE, 32U, true };
/* *INDENT-ON* */

/**
 * Registers all constants and operators for this module.
 *
 * @param[in] handle Pointer to library handle structure.
 *
 * @retval true  Successfully registered all constants and operators for this
 *               module.
 * @retval false Failed to register all constants and operators.
 */
static bool trig_register (struct lbx_handle *handle)
{
	bool ret = true;

	ret &= lbx_register_operator(handle, &ops_acos);
	ret &= lbx_register_operator(handle, &ops_acosh);
	ret &= lbx_register_operator(handle, &ops_asin);
	ret &= lbx_register_operator(handle, &ops_asinh);
	ret &= lbx_register_operator(handle, &ops_atan);
	ret &= lbx_register_operator(handle, &ops_atanh);
	ret &= lbx_register_operator(handle, &ops_atan2);
	ret &= lbx_register_operator(handle, &ops_cos);
	ret &= lbx_register_operator(handle, &ops_cosh);
	ret &= lbx_register_operator(handle, &ops_cot);
	ret &= lbx_register_operator(handle, &ops_csc);
	ret &= lbx_register_operator(handle, &ops_sec);
	ret &= lbx_register_operator(handle, &ops_sin);
	ret &= lbx_register_operator(handle, &ops_sinc);
	ret &= lbx_register_operator(handle, &ops_sinh);
	ret &= lbx_register_operator(handle, &ops_tan);
	ret &= lbx_register_operator(handle, &ops_tanh);
	ret &= lbx_register_operator(handle, &ops_rad2deg);
	ret &= lbx_register_operator(handle, &ops_deg2rad);
	ret &= lbx_register_variable(handle, "PI", &eval_pi);

	return ret;
}
