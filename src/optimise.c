/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/optimise.c
 * @brief Implementation of optimisation routines.
 */

#include "optimise.h"

#include <stdint.h>

bool optimise_constants (struct lbx_handle *handle)
{
	struct lbx_token literal = { .type = LBX_TOKEN_LITERAL };
	double           r;
	double           constants[10U];
	uint8_t          n_output = 0U, n_constants = 0U;

	for (size_t i = 0U; i < handle->output.num; i++) {
		struct lbx_token *token = &handle->output.queue[i];

		switch (token->type) {
		case LBX_TOKEN_OPERATOR:
			if (n_constants >= token->token.op.n_operands) {
				/* Evaluate the sub-expression: */
				n_constants -= token->token.op.n_operands;
				r = (*token->token.op.eval)(&constants[n_constants]);
				constants[n_constants++] = r;
				continue;
			}
			break;
		case LBX_TOKEN_LITERAL:
			constants[n_constants++] = token->token.literal;
			continue;
		case LBX_TOKEN_VARIABLE:
			break;
		default:
			return false;
		}

		/* Flush the constants back to the output queue: */
		for (size_t j = 0U; j < n_constants; j++) {
			literal.token.literal = constants[j];
			handle->output.queue[n_output++] = literal;
		}
		n_constants = 0U;

		handle->output.queue[n_output++] = *token;
	}

	/* Flush the remaining constants back to the output queue: */
	for (size_t j = 0U; j < n_constants; j++) {
		literal.token.literal = constants[j];
		handle->output.queue[n_output++] = literal;
	}

	/* Update the output queue length: */
	handle->output.num = n_output;

	return true;
}
