/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/optimise.h
 * @brief Header for optimisation routines.
 */

#pragma once

#include "handle.h"

#include <stdbool.h>

/**
 * Optimises the expression by perform constant folding.
 *
 * @param[in,out] handle Pointer to the library handle.
 *
 * @retval true  Constant folding optimisation completed successfully.
 * @retval false Failed to optimise the expression.
 */
extern bool optimise_constants (struct lbx_handle *handle);
