/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/libexpr.c
 * @brief libexpr's implementation.
 */

#include "error.h"
#include "parse.h"
#include "stack.h"
#include "str.h"

#include <libexpr.h>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Parses a special character. Special characters are '(', ')' and ','. If any
 * other character is encountered, the 'next' variable is set to point to 'str'
 * and true is returned.
 *
 * @param[in]  handle Pointer to the library handle.
 * @param[in]  str    Pointer to the null-terminated string to parse.
 * @param[out] next   Pointer to the character following the parsed token.
 *
 * @retval true  Successfully handled the input string.
 * @retval false A problem was encountered.
 *
 * @note Internal function. Caller must guarantee valid parameters.
 */
static bool parse_special (struct lbx_handle *handle,
			   char              *str,
			   char             **next)
{
	uint32_t prev_boost = handle->parser.boost;

	*next = str;

	switch (str[0]) {
	case '(':
		/* Boost the operator precedence: */
		handle->parser.boost += LBX_PRECEDENCE_MAX;

		if (prev_boost > handle->parser.boost) {
			lbx_error(handle, LBX_ERROR_PARSE, NULL, "Precedence boost overflow.");
			return false;
		}
		break;
	case ')':
		/* Un-boost the operator precedence: */
		handle->parser.boost -= LBX_PRECEDENCE_MAX;

		if (prev_boost < handle->parser.boost) {
			return false;
		}
		break;
	case ',':
		/* Ignore the character and continue parsing the next token: */
		break;
	default:
		/* Not a special character: */
		return true;
	}

	/* Return a pointer to the next character after the token: */
	*next += 1U;
	return true;
}

/**
 * Parses an operator. Valid operators are defined in the library handle. If no
 * valid operators are encountered, the 'next' variable is set to point to 'str'
 * and true is returned.
 *
 * @param[in]  handle Pointer to the library handle.
 * @param[in]  str    Pointer to the null-terminated string to parse.
 * @param[out] next   Pointer to the character following the parsed token.
 *
 * @retval true  Successfully handled the input string.
 * @retval false A problem was encountered.
 *
 * @note Internal function. Caller must guarantee valid parameters.
 */
static bool parse_operator (struct lbx_handle *handle,
			    char              *str,
			    char             **next)
{
	struct lbx_token    token = { .type = LBX_TOKEN_OPERATOR };
	struct lbx_operator top;

	*next = str;

	/* Parse operators: */
	for (size_t i = 0U; i < handle->operators.num; i++) {
		struct lbx_operator op = handle->operators.list[i];
		op.precedence += handle->parser.boost;

		if (strncmp(op.str, str, strlen(op.str)) == 0) {

			/* Ensure there isn't a longer valid operator: */
			if ((op.is_func && lbx_str_is_identifier(str, strlen(op.str) + 1U)) ||
			    lbx_str_is_operator(handle, str, strlen(op.str) + 1U)) {
				continue;
			}

			while (stack_peek(handle->parser.op_stack, &top) &&
			       (top.precedence > op.precedence ||
				(top.precedence == op.precedence && top.associativity ==
				 LBX_ASSOCIATIVITY_LEFT))) {

				/* Pop the top of the stack onto the output queue: */
				if (stack_pop(handle->parser.op_stack, &token.token.op) == false) {
					lbx_error(handle, LBX_ERROR_PARSE, NULL,
						  "Unmatched paranthesis2.");
					return false;
				}
				handle->output.queue[handle->output.num++] = token;
			}

			/* Push the operator onto the stack: */
			stack_push(handle->parser.op_stack, &op);

			/* Return a pointer to the next character after the token: */
			*next += strlen(op.str);
			break;
		}
	}

	return true;
}

/**
 * Parses a variable. Valid variables are defined in the library handle. If no
 * valid variables are encountered, the 'next' variable is set to point to 'str'
 * and true is returned.
 *
 * @param[in]  handle Pointer to the library handle.
 * @param[in]  str    Pointer to the null-terminated string to parse.
 * @param[out] next   Pointer to the character following the parsed token.
 *
 * @retval true  Successfully handled the input string.
 * @retval false A problem was encountered.
 *
 * @note Internal function. Caller must guarantee valid parameters.
 */
static bool parse_variable (struct lbx_handle *handle,
			    char              *str,
			    char             **next)
{
	struct lbx_token token = { .type = LBX_TOKEN_VARIABLE };

	*next = str;

	/* Parse variables: */
	for (size_t i = 0U; i < handle->variables.num; i++) {
		struct lbx_variable var = handle->variables.list[i];

		if (strncmp(var.str, str, strlen(var.str)) == 0) {

			/* Ensure the entire variable name is checked: */
			if (lbx_str_is_identifier(str, strlen(var.str) + 1U)) {
				continue;
			}

			token.token.var = var;
			handle->output.queue[handle->output.num++] = token;

			/* Return a pointer to the next character after the token: */
			*next += strlen(var.str);
			break;
		}
	}

	return true;
}

/**
 * Parses a numerical literal. If no valid numerical literals are encountered,
 * the 'next' variable is set to point to 'str' and true is returned.
 *
 * @param[in]  handle Pointer to the library handle.
 * @param[in]  str    Pointer to the null-terminated string to parse.
 * @param[out] next   Pointer to the character following the parsed token.
 *
 * @retval true  Successfully handled the input string.
 * @retval false A problem was encountered.
 *
 * @note Internal function. Caller must guarantee valid parameters.
 */
static bool parse_literal (struct lbx_handle *handle,
			   char              *str,
			   char             **next)
{
	*next = str;

	/* If the previous token was not a operator, treat this character as an operator: */
	if ((*str == '+' || *str == '-') && handle->parser.last != LBX_TOKEN_OPERATOR) {
		return true;
	}

	/* Else treat this character as a sign and continue parsing the literal: */
	double val = strtod(str, next);

	if (*next != str) {
		/* Push the literal to the output stack: */
		struct lbx_token literal = {
			.type          = LBX_TOKEN_LITERAL,
			.token.literal = val
		};

		handle->output.queue[handle->output.num++] = literal;
	}

	return true;
}

bool parse_token (struct lbx_handle *handle,
		  char              *str,
		  char             **next)
{
	/* Parse special characters: */
	if (parse_special(handle, str, next) == false) {
		return false;
	}
	/* Successfully parsed a special character: */
	if (*next != str) {
		return true;
	}

	/* Parse numeric literals: */
	if (parse_literal(handle, str, next) == false) {
		return false;
	}
	/* Successfully parsed a numeric literal: */
	if (*next != str) {
		handle->parser.last = LBX_TOKEN_LITERAL;
		return true;
	}

	/* Parse operators: */
	if (parse_operator(handle, str, next) == false) {
		return false;
	}
	/* Successfully parsed an operator: */
	if (*next != str) {
		handle->parser.last = LBX_TOKEN_OPERATOR;
		return true;
	}

	/* Parse variables: */
	if (parse_variable(handle, str, next) == false) {
		return false;
	}
	/* Successfully parsed a variable: */
	if (*next != str) {
		handle->parser.last = LBX_TOKEN_VARIABLE;
		return true;
	}

	/* Failed to parse the token: */
	return false;
}
