/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/parse.h
 * @brief The parser module is responsible for parsing literals, operators,
 *        variables and special characters.
 */

#pragma once

#include "handle.h"

#include <stdbool.h>

/**
 * Parses a single token. A token can be a literal, operator, variable or
 * special characters.
 *
 * @param[in]  handle Pointer to the library handle.
 * @param[in]  str    Pointer to the null-terminated string to parse.
 * @param[out] next   Pointer to the character following the parsed token.
 *
 * @retval true  A token was successfully parsed.
 * @retval false A problem was encountered or no more tokens to parse.
 */
extern bool parse_token (struct lbx_handle *handle,
			 char              *str,
			 char             **next);
