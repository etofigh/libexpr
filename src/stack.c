/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/stack.c
 * @brief Simple stack implementation.
 */

#include "stack.h"

#include <stdlib.h>
#include <string.h>

/**
 * Indicates if the stack is empty.
 *
 * @param[in] stack Pointer to the stack handle.
 *
 * @retval true  Stack is empty.
 * @retval false Stack is not empty.
 */
static bool is_empty (struct stack *stack)
{
	if (stack->top == -1) {
		return true;
	}

	return false;
}

/**
 * Indicates if the stack is full.
 *
 * @param[in] stack Pointer to the stack handle.
 *
 * @retval true  Stack is full.
 * @retval false Stack is not full.
 */
static bool is_full (struct stack *stack)
{
	if (stack->top == stack->size) {
		return true;
	}

	return false;
}

struct stack *stack_alloc (const int64_t size, const size_t el_size)
{
	struct stack *stack;

	if (size <= 0) {
		return NULL;
	}

	/* Allocate zeroed memory for the stack handle: */
	if ((stack = calloc(1U, sizeof (*stack))) == NULL) {
		return NULL;
	}

	/* Allocate zeroed memory for the stack buffer: */
	if ((stack->buf = calloc((size_t) size, el_size)) == NULL) {
		free(stack);
		return NULL;
	}

	stack->size    = size;
	stack->el_size = el_size;
	stack->top     = -1;

	return stack;
}

void stack_free (struct stack *stack)
{
	if (stack == NULL) {
		return;
	}

	/* Release the stack's buffer: */
	free(stack->buf);

	/* Release the stack object itself: */
	free(stack);
}

bool stack_peek (struct stack *stack, void *el)
{
	if (stack == NULL || is_empty(stack) || el == NULL) {
		return false;
	}

	/* Calculate the starting location for the source element: */
	void *src = (uint8_t *) stack->buf + (stack->top * stack->el_size);
	memcpy(el, src, stack->el_size);

	return true;
}

bool stack_pop (struct stack *stack, void *el)
{
	if (stack == NULL || is_empty(stack) || el == NULL) {
		return false;
	}

	/* Calculate the starting location for the source element: */
	void *src = (uint8_t *) stack->buf + (stack->top * stack->el_size);
	--stack->top;
	memcpy(el, src, stack->el_size);

	return true;
}

bool stack_push (struct stack *stack, void *el)
{
	if (stack == NULL || is_full(stack) || el == NULL) {
		return false;
	}

	++stack->top;

	/* Calculate the starting location for the target element: */
	void *dest = (uint8_t *) stack->buf + (stack->top * stack->el_size);
	memcpy(dest, el, stack->el_size);

	return true;
}

int64_t stack_size (struct stack *stack)
{
	if (stack == NULL) {
		return -1;
	}

	return stack->top + 1;
}
