/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/stack.h
 * @brief Simple stack implementation.
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * Opaque stack handle.
 */
struct stack {

	/** Maximum stack size (in elements). */
	int64_t size;

	/** Size of each element (in bytes). */
	size_t el_size;

	/** Top of the stack pointer. */
	int64_t top;

	/** Buffer that backs the stack. */
	void *buf;
};

/**
 * Creates a new (empty) stack object and allocates memory for its buffer.
 *
 * @param[in] size    The maximum size of the stack (in elements).
 * @param[in] el_size The size of each element (in bytes).
 *
 * @return NULL on failure, else a valid pointer to a newly created stack
 *         object. The caller owns this object and must call stack_free() to
 *         release its resources.
 */
extern struct stack *stack_alloc (const int64_t size, const size_t el_size);

/**
 * Destroys the stack object and frees the memory allocated for its buffer.
 *
 * @param[in] stack Pointer to the stack handle.
 *
 * @return None.
 */
extern void stack_free (struct stack *stack);

/**
 * Returns the element on top of the stack without modifying the contents of the
 * stack.
 *
 * @param[in] stack Pointer to the stack handle.
 * @param[in] el    Pointer to the element to write to from the top of the stack.
 *
 * @retval true  Successfully read the element from the stack.
 * @retval false The stack handle or element pointer are invalid or the stack is
 *               empty.
 */
extern bool stack_peek (struct stack *stack, void *el);

/**
 * Returns the element on top of the stack and removes it from the stack.
 *
 * @param[in] stack Pointer to the stack handle.
 * @param[in] el    Pointer to the element to write to from the top of the stack.
 *
 * @retval true  Successfully popped the element from the stack.
 * @retval false The stack handle or element pointer are invalid or the stack is
 *               empty.
 */
extern bool stack_pop (struct stack *stack, void *el);

/**
 * Pushes the provided element on top of the stack.
 *
 * @param[in] stack Pointer to the stack handle.
 * @param[in] el    Pointer to the element to push onto the stack.
 *
 * @retval true  Successfully pushed the element onto the stack.
 * @retval false The stack handle or element pointer are invalid or the stack is
 *               fill.
 */
extern bool stack_push (struct stack *stack, void *el);

/**
 * Returns the number of elements in the stack.
 *
 * @param[in] stack Pointer to the stack handle.
 *
 * @return The number of elements in the stack or -1  if the stack is empty.
 */
extern int64_t stack_size (struct stack *stack);
