/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/str.c
 * @brief Internal string processing functions.
 */

#include "str.h"

#include <ctype.h>
#include <string.h>

void lbx_str_trim (char *str)
{
	if (str == NULL) {
		return;
	}

	size_t count = 0U;

	for (size_t i = 0U; str[i]; i++) {
		if (isspace(str[i]) == false) {
			str[count++] = str[i];
		}
	}

	str[count] = '\0';
}

bool lbx_str_is_identifier (const char *str, const size_t n)
{
	if (str == NULL || n == 0U) {
		return false;
	}

	/* First character must be an underscore or alphabet: */
	if (str[0U] != '_' && isalpha(str[0U]) == 0) {
		return false;
	}

	/* Remaining characters must be an underscore, digit or alphabet: */
	for (size_t i = 1U; i < n; i++) {
		char c = str[i];

		if (c != '_' && isalpha(c) == 0 && isdigit(c) == 0) {
			return false;
		}
	}

	return true;
}

bool lbx_str_is_operator (struct lbx_handle *handle,
			  const char        *str,
			  const size_t       n)
{
	if (handle == NULL || str == NULL || n == 0U) {
		return false;
	}

	/* Go through all available operators: */
	for (size_t i = 0U; i < handle->operators.num; i++) {
		if (strncmp(str, handle->operators.list[i].str, n) == 0) {
			return true;
		}
	}

	return false;
}

bool lbx_str_is_variable (struct lbx_handle *handle,
			  const char        *str,
			  const size_t       n)
{
	if (handle == NULL || str == NULL || n == 0U) {
		return false;
	}

	/* Go through all available variables: */
	for (size_t i = 0U; i < handle->variables.num; i++) {
		if (strncmp(str, handle->variables.list[i].str, n) == 0) {
			return true;
		}
	}

	return false;
}
