/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/str.h
 * @brief Header file to define string processing functions.
 */

#pragma once

#include "handle.h"

#include <stdbool.h>
#include <stddef.h>

/**
 * Trims the white space characters from a string.
 *
 * @param[in,out] str Pointer to a null-terminated string to trim. The string is
 *                    modified in place.
 *
 * @return None.
 */
extern void lbx_str_trim (char *str);

/**
 * Checks whether the first 'n' characters of the string 'str' represent a valid
 * variable or function identifier.
 *
 * @param[in] str Pointer to a null-terminated string to check.
 * @param[in] n   The maximum number of characters to check. If a
 *                null-terminator is found sooner, then on fewer characters than
 *                'n' are checked.
 *
 * @retval true  The string is a valid identifier.
 * @retval false The string is not a valid identifier.
 */
extern bool lbx_str_is_identifier (const char *str, const size_t n);

/**
 * Checks whether the first 'n' characters of the string 'str' represent a valid
 * operator identifier.
 *
 * @param[in] handle Pointer to the library handle.
 * @param[in] str    Pointer to a null-terminated string to check.
 * @param[in] n      The maximum number of characters to check. If a
 *                   null-terminator is found sooner, then on fewer characters
 *                   than 'n' are checked.
 *
 * @retval true  The string is a valid operator.
 * @retval false The string is not a valid operator.
 */
extern bool lbx_str_is_operator (struct lbx_handle *handle,
				 const char        *str,
				 const size_t       n);

/**
 * Checks whether the first 'n' characters of the string 'str' represent a valid
 * variable identifier.
 *
 * @param[in] handle Pointer to the library handle.
 * @param[in] str    Pointer to a null-terminated string to check.
 * @param[in] n      The maximum number of characters to check. If a
 *                   null-terminator is found sooner, then on fewer characters
 *                   than 'n' are checked.
 *
 * @retval true  The string is a valid variable.
 * @retval false The string is not a valid variable.
 */
extern bool lbx_str_is_variable (struct lbx_handle *handle,
				 const char        *str,
				 const size_t       n);
