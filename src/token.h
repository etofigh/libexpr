/**
 * Copyright © 2018 Ehsan Tofigh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: libexpr
 *
 * @file  src/handle.h
 * @brief Header file to define library handle structure.
 */

#pragma once

#include "variable.h"

#include <operator.h>

/**
 * Token type.
 */
enum lbx_token_type {
	LBX_TOKEN_LITERAL,
	LBX_TOKEN_OPERATOR,
	LBX_TOKEN_VARIABLE,

	LBX_TOKEN_COUNT
};

/**
 * Structure to define a token.
 */
struct lbx_token {

	/** The token type. */
	enum lbx_token_type type;

	/**
	 * Union of all possible token types. The correct type can be used without
	 * casting by first inspecting the token type.
	 */
	union {
		double              literal;
		struct lbx_operator op;
		struct lbx_variable var;
	} token;
};
